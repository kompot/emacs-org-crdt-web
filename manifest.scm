(specifications->manifest
  (list ;; Base packages
        "nginx"
        "bash-minimal"
        "glibc-locales"
        "nss-certs"

        ;; CMDline
        "coreutils"

        ;; Emacs!
        "emacs"
        "emacs-crdt"))
