(use-modules (gnu))

(use-package-modules web emacs)
;(use-service-modules ...)

(operating-system
  (host-name "emacs-crdt-blog")
  (timezone "Europe/Ljubljana")
  (locale "sl_SI.UTF-8")
  (file-systems (cons (file-system
                        (device  (file-system-label "r00t"))
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))
  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (targets '("/dev/sdX"))))
  (packages (append (list (specification->package "nginx")
                          (specification->package "emacs")
                          (specification->package "emacs-crdt"))
            %base-packages))
  (services %base-services))

